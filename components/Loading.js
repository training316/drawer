import  React from 'react'

import {useState, useEffect} from 'react'
import { useRouter } from 'next/router';

import Box from "@mui/material/Box"
import CircularProgress from '@mui/material/CircularProgress';

const Loading = () => {
    const router = useRouter();

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const handleStart = (url) => setLoading(true);
        const handleComplete = (url) => setLoading(false);

        router.events.on('routeChangeStart', handleStart)
        router.events.on('routeChangeComplete', handleComplete)
        router.events.on('routeChangeError', handleComplete)

        return () => {
            router.events.off('routeChangeStart', handleStart)
            router.events.off('routeChangeComplete', handleComplete)
            router.events.off('routeChangeError', handleComplete)
        }
    }, []);
    
    return loading && (
        <Box sx={{
            position: "fixed",
            right: "5%",
            bottom: "5%"
        }}>
            <CircularProgress sx={{color: 'black'}} />
        </Box>
    );
}

export default Loading