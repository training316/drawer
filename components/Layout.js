import {useEffect, useState} from 'react'
import Head from 'next/head';
import MuiNav from './MuiNav';
import MyDrawer from './MyDrawer';
import CssBaseline from '@mui/material/CssBaseline';
import { drawerMinWidth, drawerWidth } from '../config/drawer-setting';

import  Box  from '@mui/material/Box'

const Layout = ({children}) => {

    const [open, setOpen] = useState(true)

    const toggleOpen = () =>{
        setOpen(!open);
    }

    return (
        <>
            <Head>
                <title>Demo Drawer</title>
            </Head>
            <Box sx={{display: "flex"}}>
                <CssBaseline />
                <MyDrawer open={open}/>
                <Box
                    sx={{
                        marginLeft: `${open ? drawerWidth : drawerMinWidth}px`,
                        transition: 'margin-left 0.195s'
                    }}
                >
                    <MuiNav toggleDrawer={toggleOpen} open={open}></MuiNav>
                    <Box component="main" 
                        sx={{ 
                            flexGrow: 1, 
                            p: 3,
                        }} 
                    >
                        {children}
                    </Box>
                </Box>
            </Box>
            {/* {children} */}
        </>
    );
}

export default Layout;