import React from 'react'
import { styled } from '@mui/material/styles';
import MuiDrawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';

import { drawerWidth, drawerMinWidth } from '../config/drawer-setting';

const openedMixin = (theme) => ({
    width: drawerWidth,
    transition: theme.transitions.create(['width', 'position'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: 'hidden',
    '& .MuiListItemText-root':{
        opacity:1
    },
    '& .MuiListItemIcon-root':{
        marginRight: theme.spacing(3)
    }
});

const closedMixin = (theme) => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: drawerMinWidth,
    '& .MuiListItemText-root':{
        opacity:0
    },
    '& .MuiListItemIcon-root':{
        marginRight: theme.spacing(0)
    }
});



const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })
(({ theme, open }) => ({
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        position: 'fixed',
        zIndex: 12000,
        ...(open && {
            ...openedMixin(theme),
            '& .MuiDrawer-paper': openedMixin(theme),
        }),
        ...(!open && {
            ...closedMixin(theme),
            '& .MuiDrawer-paper': closedMixin(theme),
        }),
        background: 'red',
        '&:hover':{
            ...openedMixin(theme),
            '& .MuiDrawer-paper': openedMixin(theme),
        }
    }),
);

const MyListItemText = styled(ListItemText)(({ theme }) => ({
        transition: theme.transitions.create('opacity', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        })
    }),
);

const MyListItemIcon = styled(ListItemIcon)(({ theme }) => ({
        transition: theme.transitions.create('margin-right', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        })
    }),
);

const MyDrawer = ({open}) => {
    return (    
        <Drawer variant="permanent" open={open}>
            <Divider />
            <List>
                {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
                    <ListItemButton
                        key={text}
                        sx={{
                            minHeight: 48,
                            justifyContent: open ? 'initial' : 'center',
                            px: 2.5,
                            
                        }}
                    >
                        <MyListItemIcon
                            sx={{
                                minWidth: 0,
                                justifyContent: 'center',
                            }}
                        >
                            {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                        </MyListItemIcon>
                        <MyListItemText primary={text} />
                    </ListItemButton>
                ))}
                </List>
                <Divider />
                <List>
                    {['All mail', 'Trash', 'Spam'].map((text, index) => (
                        <ListItemButton
                        key={text}
                        sx={{
                            minHeight: 48,
                            justifyContent: open ? 'initial' : 'center',
                            px: 2.5,
                        }}
                        >
                            <MyListItemIcon
                                sx={{
                                    minWidth: 0,
                                    justifyContent: 'center',
                                }}
                            >
                                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                            </MyListItemIcon>
                            <MyListItemText primary={text} />
                        </ListItemButton>
                    ))}
            </List>
      </Drawer>
    )
}

export default MyDrawer